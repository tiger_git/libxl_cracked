# libxl_cracked

#### 项目介绍
使用libxl读取Excel文件，win32 和 linux 双平台均已经实现破解
本项目仅用于学习交流，如需在生产环境中使用libxl，请够购买正版。

更新一串神秘代码：

```c++
static const std::string libxl_cracked_name = "mylibxl";
#if defined(WIN32) || defined(WIN64)
static const std::string libxl_cracked_key = "windows-2f2129060dcce70c67ba606aa8jdk3h8";
#else
static const std::string libxl_cracked_key = "linux-2f2129060dcce70c67ba606aa8idk3g8";
#endif // WIN32
```
